#HeartBeat
<div>
    <p>
     心跳检测应用服务器(如Tomcat,Jetty)的JAVA WEB应用程序.
    </p>
    <p>
     如何实现?
     <br/>
     使用HttpClient对指定的服务器(application-instance) URL 按频率(10秒,20秒...) 发起请求并记录响应的信息(连接耗时,是否连接成功,是否有异常,响应数据包大小),
     若检测到不正常(响应码不是200,抛出异常...)时则发送邮件给指定的地址,当检测恢复正常时也发送提醒邮件.
     <br/>
     将来会添加更多的实时提醒方式接口,如微信,短信
    </p>
</div>

<div>
    <h3>框架及版本</h3>
    <ul>
        <li>Spring Framework - 3.2.2.RELEASE</li>
        <li>Quartz - 2.2.1</li>
        <li>Hibernate - 4.1.7.Final</li>
        <li><a href="http://www.bootcss.com/p/flat-ui/">Flat UI</a></li>
        <li>Maven - 3.1.0</li>
    </ul>
</div>

<div>
    <h3>特点</h3>
    <ul>
        <li>无侵入</li>
        <li>独立部署</li>
        <li>可同时监测多个应用服务器</li>
        <li>使用简洁,灵活</li>
        <li>提醒方式及时,多样(目前仅实现邮件提醒,将来会加入微信提醒,短信提醒等)</li>
    </ul>
</div>

<div>
    <h3>运行环境</h3>
    <ul>
        <li>JRE 1.7 +</li>
        <li>MySql 5.5 +</li>
        <li>Tomcat 7 +</li>
    </ul>
</div>

<div>
    <h3>如何使用?</h3>
    <ol>
        <li>项目是Maven管理的, 需要在电脑上安装maven(开发用的版本号为3.1.0), MySql(开发用的版本号为5.5)</li>
        <li>下载(或clone)项目到本地</li>
        <li>
            创建MySQL数据库(默认数据库名:heart_beat), 并运行相应的SQL脚本(脚本文件位于others/database目录),
            <br/>
            运行脚本的顺序: HeartBeat.ddl -> quartz_mysql_innodb.sql
        </li>
        <li>
            修改HeartBeat.properties(位于src/main/resources目录)中的数据库连接信息(包括username, password等)
            <br/>
            <strong>NOTE: 为了确保能收到提醒邮件,请将配置文件中的 <em>mail.develop.address</em> 配置为你的邮件地址;
            若在生产环境,请将 <em>mail.develop.environment</em> 值修改为 false (true表示为开发环境)</strong>
        </li>
        <li>
            将本地项目导入到IDE(如Intellij IDEA)中,配置Tomcat(或类似的servlet运行服务器), 并启动Tomcat(默认端口为8080)
            <br/>
               另: 也可通过maven package命令将项目编译为war文件(HeartBeat.war),
                     将war放在Tomcat中并启动(注意: 这种方式需要将HeartBeat.properties加入到classpath中并正确配置数据库连接信息).
        </li>
    </ol>
</div>

<div>
    <h3>Change-Log</h3>
    <ol>
        <li>
            <p>
                2014-10-17   ----    Initial project
            </p>
        </li>
        <li>
            <p>
                2015-02-13   ----    Move development to <a href="https://coding.net/u/monkeyk/p/HeartBeat/git">coding.net</a>
            </p>
        </li>
        <li>
            <p>
                2015-03-01   ----    Back to OSC and update documents; Add 0.1 branch
            </p>
        </li>
        <li>
            <p>
                2015-03-14   ----    Monitoring log add response data size;Add list of monitoring reminder logs; Update page styles; Add 0.2 branch
            </p>
        </li>
    </ol>
</div>


<div>
    <h3>程序运行主要截图</h3>
    <ol>
        <li>
            <p>
                Monitoring
                <br/>
                <img src="http://andaily.qiniudn.com/hbmonitoring_0.1.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Monitoring details
                <br/>
                <img src="http://andaily.qiniudn.com/hbmonitoring-details_0.1.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Overview
                <br/>
                <img src="http://andaily.qiniudn.com/hbinstances_0.1.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Create
                <br/>
                <img src="http://andaily.qiniudn.com/hbnew-instance_0.1.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Monitoring-Log
                <br/>
                <img src="http://andaily.qiniudn.com/hbhb-log_0.1.png" alt="hb"/>
                <br/>
            </p>
        </li>
    </ol>
</div>




<hr/>
<div>
    More Open-Source projects see <a href="http://andaily.com/my_projects.html">http://andaily.com/my_projects.html</a>
    <br/>
    From <a href="http://andaily.com">andaily.com</a>
    <br/>
    Email <a href="mailto:monkeyk@shengzhaoli.com">monkeyk@shengzhaoli.com</a>
</div>