package com.andaily.infrastructure.hibernate;

import com.andaily.domain.user.User;
import com.andaily.domain.user.UserRepository;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Shengzhao Li
 */
@Repository("userRepository")
public class UserRepositoryHibernate extends AbstractRepositoryHibernate<User> implements UserRepository {

    @Override
    public User findByUsername(String username) {
        final List<User> list = find("from User u where u.username = :username and u.archived = false", ImmutableMap.of("username", username));
        return list.isEmpty() ? null : list.get(0);
    }
}