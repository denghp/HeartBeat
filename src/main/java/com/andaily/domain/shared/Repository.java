/*
 * Copyright (c) 2013 Honyee Industry Group Co., Ltd
 * www.honyee.biz
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Honyee Industry Group Co., Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Honyee Industry Group Co., Ltd.
 */
package com.andaily.domain.shared;

import com.andaily.domain.AbstractDomain;

import java.util.Collection;
import java.util.List;

/**
 * @author Shengzhao Li
 */

public interface Repository {

    <T extends AbstractDomain> T findById(Integer id, Class<T> clazz);

    <T extends AbstractDomain> T findByGuid(String guid, Class<T> clazz);

    <T extends AbstractDomain> void saveOrUpdate(T domain);

    <T extends AbstractDomain> void saveOrUpdateAll(Collection<T> collection);

    <T extends AbstractDomain> void delete(T domain);

    <T extends AbstractDomain> void deleteByGuid(Class<T> clazz, String guid);

    <T extends AbstractDomain> void deleteAll(Collection<T> elements);

    <T extends AbstractDomain> List<T> findAll(Class<T> clazz, boolean active);

    <T extends AbstractDomain> List<T> findByGuids(Class<T> clazz, List<String> guids);

}