package com.andaily.infrastructure.hibernate;

import com.andaily.domain.user.User;
import com.andaily.domain.user.UserRepository;
import com.andaily.infrastructure.AbstractRepositoryTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;

/**
 * @author Shengzhao Li
 */
public class UserRepositoryHibernateTest extends AbstractRepositoryTest {

    @Autowired
    private UserRepository userRepository;


    @Test
    public void findByGuid() throws Exception {

        User user = userRepository.findByGuid("oood", User.class);
        assertNull(user);

        user = new User("user", "123", "123", "ewo@honyee.cc");
        userRepository.saveOrUpdate(user);

        user = userRepository.findByGuid(user.guid(), User.class);
        assertNotNull(user);
        assertNotNull(user.email());


        final User user2 = userRepository.findByUsername(user.username());
        assertNotNull(user2);
    }


}